<?php
/**
 * Created by PhpStorm.
 * User: Mario Zárate Velásquez
 * Email: mario.zarate@ucsp.edu.pe
 */

use App\Controllers\HomeController;

$app->get('/', HomeController::class.':indexAction')->setName('home');