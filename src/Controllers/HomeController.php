<?php
/**
 * Created by PhpStorm.
 * User: Mario Zárate Velásquez
 * Email: mario.zarate@ucsp.edu.pe
 */

namespace App\Controllers;

use Slim\Http\Request;
use Slim\Http\Response;

class HomeController extends BaseController
{

    public function indexAction(Request $request, Response $response, $args)
    {
        $this->logger->info("Index action");

        $this->flash->addMessage('info', 'Sample flash message');
        $messages = $this->flash->getMessage('info');
        $args['flash'] = $messages;

        return $this->view->render($response, 'home.twig', $args);
    }

}