<?php
/**
 * Created by PhpStorm.
 * User: Mario Zárate Velásquez
 * Email: mario.zarate@ucsp.edu.pe
 */

namespace App\Controllers;

use Slim\Container;

class BaseController
{
    protected $view;
    protected $logger;
    protected $flash;
    protected $em;

    public function __construct(Container $c)
    {
        $this->view = $c->get('view');
        $this->logger = $c->get('logger');
        $this->flash = $c->get('flash');
        $this->em = $c->get('em');
        $this->settings = $c->get('settings');
    }
}
