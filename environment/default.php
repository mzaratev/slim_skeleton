<?php
/**
 * Created by PhpStorm.
 * User: Mario Zárate Velásquez
 * Email: mario.zarate@ucsp.edu.pe
 */

return [
    'settings' => [

        'env' => 'default',

        'displayErrorDetails' => true,

        // twig settings
        'view' => [
            'template_path' => __DIR__ . '/../frontend/Views',
            'twig' => [
                'cache' => false,
                'debug' => true,
                'auto_reload' => true,
            ],
        ],

        // doctrine settings
        'doctrine' => [
            'meta' => [
                'entity_path' => [
                    __DIR__ . '/../src/Entities'
                ],
                'auto_generate_proxies' => true,
                'proxy_dir' =>  __DIR__.'/../var/cache/doctrine_proxies',
                'cache' => null,
            ],
            'defaultDatabaseOptions' => [
                'charset' => 'utf8',
                'collate' => 'utf8_general_ci'
            ],
            'connection' => []
        ],

        // monolog settings
        'logger' => [
            'name' => 'app',
            'path' => __DIR__ . '/../log/app.log',
        ],
    ]
];