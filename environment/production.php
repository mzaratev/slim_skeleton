<?php
/**
 * Created by PhpStorm.
 * User: Mario Zárate Velásquez
 * Email: mario.zarate@ucsp.edu.pe
 */

return [
    'settings' => [

        'env' => 'prod',

        'displayErrorDetails' => true,

        'view' => [
            'twig' => [
                'cache' => __DIR__ . '/../var/cache/twig'
            ]
        ],

        'doctrine' => [
            'connection' => [
                'driver'   => 'pdo_mysql',
                'host'     => '127.0.0.1',
                'port'     => 8889,
                'dbname'   => 'prod_dbname',
                'user'     => 'root',
                'password' => 'root',
            ]

        ]
    ]
];