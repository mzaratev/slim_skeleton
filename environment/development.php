<?php
/**
 * Created by PhpStorm.
 * User: Mario Zárate Velásquez
 * Email: mario.zarate@ucsp.edu.pe
 */

return [
    'settings' => [

        'env' => 'dev',

        'displayErrorDetails' => true,

        'view' => [
            'twig' => [
                'cache' => false
            ]
        ],

        'doctrine' => [
            'connection' => [
                'driver'   => 'pdo_mysql',
                'host'     => '127.0.0.1',
                'port'     => 8889,
                'dbname'   => 'dev_dbname',
                'user'     => 'root',
                'password' => 'root',
            ]

        ]
    ]
];
