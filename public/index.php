<?php
/**
 * Created by PhpStorm.
 * User: Mario Zárate Velásquez
 * Email: mario.zarate@ucsp.edu.pe
 */

require __DIR__ . '/../vendor/autoload.php';

session_start();
date_default_timezone_set('America/Lima');
error_reporting(E_ALL);
ini_set('display_errors', 'On');

// Setting environment
$env_settings = function() {
    $config = new \UserFrosting\Config\Config();
    $config->setPaths([
        __DIR__ . '/../environment'
    ]);
    $config->loadConfigurationFiles('development');
    return $config;
};

// Instantiate the app
$settings['settings'] = $env_settings()['settings'];

$app = new \Slim\App($settings);

// Set up dependencies
require __DIR__ . '/../config/dependencies.php';

// Register routes
require __DIR__ . '/../src/Routes/routes.php';

// Run!
$app->run();