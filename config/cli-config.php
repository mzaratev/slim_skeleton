<?php
/**
 * Created by PhpStorm.
 * User: Mario Zárate Velásquez
 * Email: mario.zarate@ucsp.edu.pe
 */

use Doctrine\ORM\Tools\Console\ConsoleRunner;
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

$settings = function() {
    $config = new \UserFrosting\Config\Config();
    $config->setPaths([
        __DIR__ . '/../environment'
    ]);
    $config->loadConfigurationFiles('development');
    return $config;
};

$settings = $settings();

$config = Setup::createAnnotationMetadataConfiguration(
    $settings['settings']['doctrine']['meta']['entity_path'],
    $settings['settings']['doctrine']['meta']['auto_generate_proxies'],
    $settings['settings']['doctrine']['meta']['proxy_dir'],
    $settings['settings']['doctrine']['meta']['cache'],
    false
);

$em = EntityManager::create($settings['settings']['doctrine']['connection'], $config);

return ConsoleRunner::createHelperSet($em);